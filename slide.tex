\documentclass[luatex, unicode, 11pt, aspectratio=1610, t, xcolor={svgnames}]{beamer}  % 左詰めにするには fleqn を指定．

% LuaLaTeX関係。
\usepackage[no-math]{fontspec}
\usepackage{luatexja}
\usepackage{luatexja-otf}
\usepackage[deluxe]{luatexja-fontspec}
\setmainjfont{IPAexMincho}[
  BoldFont=MogaMincho-Bold
]
\setsansjfont{IPAexGothic}[
  BoldFont=MigMix-2P-Bold
]
\usepackage{unicode-math}
\unimathsetup{math-style=TeX, bold-style=ISO}
%\setmainfont{XITS}
%\setmathfont{xits-math.otf}

% Beamer関係。
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{theorems}[numbered]
\renewcommand{\kanjifamilydefault}{\gtdefault}
\renewcommand{\familydefault}{\sfdefault}
\usefonttheme[onlymath]{serif}
\usetheme{Frankfurt}
\setbeamersize{text margin left=.75em, text margin right=.75em}

% Babel (日本語の場合のみ・英語の場合は不要)
% 参考：http://www.opt.mist.i.u-tokyo.ac.jp/~tasuku/beamer.html
\uselanguage{japanese}
\languagepath{japanese}
\deftranslation[to=japanese]{Theorem}{定理}
\deftranslation[to=japanese]{Lemma}{補題}
\deftranslation[to=japanese]{Proposition}{命題}
\deftranslation[to=japanese]{Example}{例}
\deftranslation[to=japanese]{Examples}{例}
\deftranslation[to=japanese]{Definition}{定義}
\deftranslation[to=japanese]{Definitions}{定義}
\deftranslation[to=japanese]{Problem}{問題}
\deftranslation[to=japanese]{Solution}{解}
\deftranslation[to=japanese]{Fact}{事実}
\deftranslation[to=japanese]{Proof}{証明}
\def\proofname{証明}

% その他のパッケージ。
\input{settings.tex}
\usepackage{smartdiagram}
%\usepackage{nccmath}  % 数式の左寄せ。
\usepackage{tikz-lace}
\tikzset{
  perc event/.style={
    decoration={snake, amplitude=1pt, segment length=3pt}
  },
  double connection/.style={
    pattern={north east lines},
    pattern color=green
  }
}

\title{最近接有向パーコレーションに対するレース展開}
\institute[NCTS]{ポスドク，國家理論科學研究中心 (NCTS)，台湾}
\author[上島芳倫]{上島芳倫\\ 共同研究者：陳隆奇 教授（國立政治大學，台湾）・半田悟 氏 (atama plus Inc.)}
\date[2021/10/16]{無限粒子系、確率場の諸問題XVI\\ 令和3年10月16日}

\begin{document}

\maketitle

\begin{frame}{概略}
  \tableofcontents
\end{frame}

\section{導入}

\begin{frame}{有向パーコレーションの定義}
  \begin{columns}[T]
    \begin{column}{0.47\textwidth}
      \begin{adjustbox}{width=\textwidth}
        \includegraphics{oriented-percolation.pdf}
      \end{adjustbox}
    \end{column}
    \begin{column}{0.53\textwidth}
      \setlength{\leftmargini}{1em}
      \begin{itemize}
        \item 最近接点の集合 $\mathscr{N}\subset\mathbb{Z}^d$，これの平行移動によって生成される集合 $\mathbb{L}^d\ni o=(0, \dots, 0)$．
        \item 時間 $\mathbb{Z}_+=\mathbb{N}\cup\{0\}$，時空間の辺集合 $\mathscr{B}^d = \Set{\bigl((x, t), (y, t+1)\bigr)\subset\mathbb{L}^d\times\mathbb{Z}_+ | y - x\in\mathscr{N}}$．
        \item 辺の占有確率 $\displaystyle \textcolor{blue}{q_p(x, t)} = \frac{p \KroneckerDelta{t}{1}}{\card{\mathscr{N}}} \indicator{\{x\in\mathscr{N}\}}$．
        \item 標本空間 $\Omega=\set{0, 1}^{\mathscr{B}^d}$．$p\in\interval{0}{\card{\mathscr{N}}}$ に対して，
          確率測度 $\mathbb{P}_p = \bigotimes_{(\symbf{x}, \symbf{y})\in\mathscr{B}^d}\mathrm{Ber}\qty\big(q_p(\symbf{y} - \symbf{x}))$．
      \end{itemize}
    \end{column}
  \end{columns}

  \bigskip
  $\symbf{b}=(\symbf{x}, \symbf{y})\in\mathscr{B}^d$ に対して，$\underline{\symbf{b}}=\symbf{x}$ および $\overline{\symbf{b}}=\symbf{y}$ と書く．$\omega\in\Omega$ と $\symbf{x}, \symbf{y}\in\mathbb{L}^d\times\mathbb{Z}_+$ に対して，
  \[
    \symbf{x}\longrightarrow\symbf{y} \xLeftrightarrow{\mathrm{def}.}
      % n=\tau(\symbf{y}) - \tau(\symbf{x})
      \exists \set{\symbf{b}_1, \dots, \symbf{b}_n}\subset \omega^{-1}(1) \qq{s.t.} \underline{\symbf{b}_1} = \symbf{x},\ \overline{\symbf{b}_n} = \symbf{y}\ \&\ \forall i,\ \overline{\symbf{b}_i} = \underline{\symbf{b}_{i+1}}.
  \]
  二点関数と感受率
  \[
    \textcolor{blue}{\varphi_p(\symbf{x})} \coloneqq \mathbb{P}_p(\symbf{o} \longrightarrow \symbf{x}),
    \quad
    \textcolor{blue}{\chi_p} \coloneqq \sum\limits_{\symbf{x}\in\mathbb{L}^d\times\mathbb{Z}_+} \varphi_p(\symbf{x}) = \mathbb{E}_p\qty\Big[\Card{\Set{ \symbf{x}\in\mathbb{L}^d\times\mathbb{Z}_+ | \symbf{o} \longrightarrow \symbf{x}}}]
  \]
\end{frame}

\begin{frame}{動機\――平均場臨界指数}
  \vspace*{-2ex}
  \begin{columns}[T]
    \begin{column}{0.60\textwidth}
      時空間の畳込み $(f\Conv g)(\symbf{x}) = \sum\limits_{\symbf{y}\in\mathbb{L}^d\times\mathbb{Z}_+} f(\symbf{y}) g(\symbf{x} - \symbf{y})$．
      \setlength{\leftmargini}{1em}
      \begin{itemize}
        \item 臨界点 $\textcolor{ForestGreen}{p_\mathrm{c}} \coloneqq \sup\set{p\in\interval{0}{\card{\mathscr{N}}} | \chi_p < \infty}$，
        \item 臨界指数 $\gamma$: $\textcolor{red}{\chi_p} \underset{p\uparrow p_\mathrm{c}}{\asymp} (p_\mathrm{c} - p)^{-\gamma}$．\\
        \textuparrow{} $d>\textcolor{ForestGreen}{d_\mathrm{c}}=4$ で $\gamma = 1$ と\textbf{予想}されている．
      \end{itemize}

      \smallskip
      \fbox{平均場臨界指数}\hspace{0.5em}
      $\mu\in\interval[open right]{0}{1}$ と $x\in\mathbb{L}^d$ に対して，RWの二点関数を $Q_\mu(x, t) \coloneqq \sum_{n=0}^{\infty} q_\mu^{\Conv n}(x, t)$ とすると，
      \[
        \textcolor{blue}{\chi_\mu^\mathrm{RW}} = \sum_{(x, t)\in\mathbb{L}^d\times\mathbb{Z}_+} Q_\mu(x, t) = \frac{1}{1 - \mu}.
      \]
    \end{column}
    \begin{column}{0.38\textwidth}
      \begin{adjustbox}{width=\textwidth}
        \includegraphics{susceptibility.pdf}
      \end{adjustbox}
      ※RWの臨界点 $p_\mathrm{c}^\mathrm{RW} = 1$．
    \end{column}
  \end{columns}

  \medskip
  \fbox{トライアングル条件}\hspace{0.5em}
  [Barsky-Aizenman, 1991] ([Aizenman-Newman, 1984] も参照)
  \[
    \sup_{\norm{\symbf{x}}_2 \geq R} \sum_{\symbf{y}\in\mathbb{L}^d\times\mathbb{Z}_+} \textcolor{red}{\varphi_{p_\mathrm{c}}^{\Conv 2}(\symbf{y})} \textcolor{blue}{\varphi_{p_\mathrm{c}}(\symbf{y} - \symbf{x})}
    = \sup_{\norm{\symbf{x}}_2 \geq R} \mathord{\begin{tikzpicture}[op diagram]
      \coordinate (o) at (0,0);
      \coordinate (x) at (2,0);
      \coordinate (w) at (0.3,1.7);
      \coordinate (y) at (2,2);
      \draw[red] (o) -- (w) -- (y);
      \draw[blue] (y) -- (x);
      \node[vertex, label={[label distance=-2pt]below:$\symbf{o}$}] at (o) {};
      \node[black, vertex] at (w) {};
      \node[black, vertex] at (y) {};
      \path (x) node[vertex] {} node[anchor=north] {$\symbf{x}$};
    \end{tikzpicture}}
    \xrightarrow[R\to\infty]{} 0
    \implies \gamma = 1.
  \]
\end{frame}

\begin{frame}{主結果\――赤外評価}
  \vspace*{-3ex}
  \begin{alertblock}{定理~(赤外評価 [Chen-Handa-K., arXiv:2106.14211])}
    単純立方格子 $\mathbb{Z}^{d\geq \textcolor{ForestGreen}{183}}\times\mathbb{Z}_+$ および\structure{体心立方格子} $\mathbb{L}^{d\geq \textcolor{ForestGreen}{9}}\times\mathbb{Z}_+$ 上の最近接有向パーコレーションに於て，$\exists K\in\interval[open]{0}{\infty}$ s.t.
    $\forall p\in\interval[open right]{0}{p_\mathrm{c}}$,
    $\forall k\in\interval{-\pi}{\pi}^d$, $\forall z\in\mathbb{C}\colon \abs{z}\in\interval[open right]{1}{m_p}$,
    \begin{equation*}
      \abs{\flt\varphi_p(k, z)}
      \leq K \abs{\flt Q_1(k, \mu_p(z))}
      = \frac{\textcolor{black}{K}}{\abs*{1 - \flt q_1(k, \mu_p(z))}}.
    \end{equation*}
  \end{alertblock}
  $k\in\interval{-\pi}{\pi}^d$ と $z\in\mathbb{C}$ に対して，Fourier-Laplace変換 $\flt \varphi_p(k, z) = \sum_{(x, t)\in\mathbb{L}^d\times\mathbb{Z}_+} \varphi_p(x, t) \Napier^{\imag k\cdot x} z^t$，
  そのLaplace変換の収束半径 $m_p$．
  また，$\mu_p(z) = (1 - \flt\varphi_p(0, \abs{z})^{-1}) \Napier^{\imag\arg z}$ ($\abs{\mu_p(z)}\in\interval[open right]{0}{1}$)．

  \medskip
  \fbox{先行研究}\hspace{0.5em}
  [Nguyen-Yang, 1993] によると，$\mathbb{Z}^d$ 上の
    $\begin{cases*}
      \hspace{-1em} & 最近接モデルの場合は $d\gg 4$ \alert{(?)}，\\
      \hspace{-1em} & spread-outモデルの場合は $d>4$．
    \end{cases*}$

  \begin{center}
    \smartdiagramset{
      module x sep=3.2,
      text width=2.3cm
    }
    \smartdiagram[flow diagram:horizontal]{{Weak bound $g(p, m) \leq K$},レース展開,{$g(p, m)$ の上界},{RW量の評価},{Stronger bound $g(p, m) < K$}}
  \end{center}
\end{frame}

\section[レース展開]{レース展開}

\begin{frame}{レース展開}
  \vspace{-3ex}
  \begin{block}{命題~([Nguyen-Yang, 1993] あるいは [Sakai, 2001])}
    $p < p_\mathrm{c}$ に対して，展開係数 $\Pi_p(\symbf{x}) = \sum_{N=0}^{\infty} (-1)^N \pi_p^{(N)}(\symbf{x})$ が存在して，
    \[
      \varphi_p(\symbf{x}) = \KroneckerDelta{\symbf{o}}{\symbf{x}} + \Pi_p(\symbf{x}) + \qty\Big(\qty(\KroneckerDelta{\symbf{o}}{\cdot} + \Pi_p) \Conv q_p \Conv \varphi_p)(\symbf{x}).
    \]
  \end{block}
  \fbox{参考}\hspace{0.5em}
  $\mu\in\interval[open right]{0}{1}$ に対して，RWでは
  $Q_\mu(\symbf{x}) = \KroneckerDelta{\symbf{o}}{\symbf{x}} + (q_\mu\Conv Q_\mu)(\symbf{x})$．

  \vspace*{-1.5ex}
  \[
    \KroneckerDelta{\symbf{o}}{\symbf{x}} + \pi_p^{(0)}(\symbf{x}) = \mathbb{P}_p\qty(
      \begin{tikzpicture}[op diagram, perc event]
        \draw[double connection, decorate]
          (0,0) to[out=30, in=330] (0,2.4);
        \draw[double connection, decorate]
          (0,0) to[out=150, in=210] (0,2.4);
        \path (0,2.4) node[vertex] {} node[anchor=south] {$\symbf{x}$};
        \path (0,0) node[vertex] {} node[anchor=north] {$\symbf{o}$};
      \end{tikzpicture}
    )
    \leq
    \begin{tikzpicture}[op diagram]
      \laceDraw[out=60, in=120, relative] (0,0) (0,2.4);
      \laceDraw[out=-60, in=-120, relative] (0,0) (0,2.4);
      \lacePutLabel[anchor=north] (0,0) {$\symbf{o}$};
      \lacePutLabel[anchor=south] (0,2.4) {$\symbf{x}$};
    \end{tikzpicture}
    \qcomma
    \pi_p^{(1)}(\symbf{x}) = \mathbb{P}_p\qty(
      \begin{tikzpicture}[op diagram, perc event]
        \draw[double connection, decorate]
          (0,0) to[out=30, in=330] coordinate[midway] (w) (0,1.6);
        \draw[double connection, decorate]
          (0,0) to[out=150, in=210] (0,1.6);
        \path node[vertex] {} node[anchor=north] {$\symbf{o}$};
        \draw[decorate] (0,1.6) -- (0,3.2);
        \draw[decorate] (w) to[out=60, in=330] (0,3.2) node[vertex] {} node[anchor=south] {$\symbf{x}$};
      \end{tikzpicture}
    )
    \leq
    \begin{tikzpicture}[op diagram]
      \laceDraw (0,0) (-1,1.6);
      \laceDraw (0,0) -- (1,1.2);
      \draw (1,1.2) -- (-1,1.6);
      \laceDraw[first=1] (-1,1.6) (0,2.8);
      \laceDraw[first=1] (1,1.2) (0,2.8);
      \lacePutLabel (1,1.2) {};
      \lacePutLabel (-1,1.6) {};
      \lacePutLabel[anchor=north] (0,0) {$\symbf{o}$};
      \lacePutLabel[anchor=south] (0,2.8) {$\symbf{x}$};
    \end{tikzpicture}
    \qcomma
    \qty(q_p \Conv \varphi_p)(\vb*{x}) =
    \begin{tikzpicture}[op diagram]
      \laceDraw[first=1] (0,0) (0,2);
      \lacePutLabel[anchor=north] (0,0) {$\symbf{o}$};
      \lacePutLabel[anchor=south] (0,2) {$\symbf{x}$};
    \end{tikzpicture}
  \]
  \vspace{-1.5ex}
  \begin{block}{命題~(van den Berg-Kesten, 1985, 簡易版)}
    \setlength\abovedisplayskip{0pt}\setlength\belowdisplayskip{0pt}
    \[
      \mathbb{P}_p\qty(
        \begin{tikzpicture}[op diagram, perc event]
          \coordinate (x) at (0,0);
          \coordinate (y) at ($(x) + (75:2.7)$);
          \coordinate (u) at (6.5,0);
          \coordinate (v) at ($(u) + (150:2.7)$);
          \draw[decorate] (x) node[vertex={left}{$\symbf{x}$}] {} -- ++(y) node[vertex={left}{$\symbf{y}$}] {};
          \draw[dashed, thick, red] ($(x) + (-1,-0.2)$) rectangle ($(y) + (0.2,0.3)$);
          \draw[decorate] (u) node[vertex={above}{$\symbf{u}$}] {} -- (v) node[vertex={above}{$\symbf{v}$}] {};
          \draw[dashed, thick, red] ($(u) + (0.3,-0.2)$) rectangle ($(v) + (-0.3,1)$);
          \draw[densely dashed, stealth-stealth, red] ($(y) + (0.2,-1.3)$) -- node[midway, sloped, below] {avoid} ($(v) + (-0.3,-0.5)$);
        \end{tikzpicture}
      )
      \leq
      \mathbb{P}_p\qty(
        \begin{tikzpicture}[op diagram, perc event]
          \coordinate (x) at (0,0);
          \coordinate (y) at ($(x) + (75:2.7)$);
          \draw[decorate] (x) node[vertex={left}{$\symbf{x}$}] {} -- ++(y) node[vertex={right}{$\symbf{y}$}] {};
        \end{tikzpicture}
      )
      \mathbb{P}_p\qty(
        \begin{tikzpicture}[op diagram, perc event]
          \coordinate (u) at (5,0);
          \coordinate (v) at ($(u) + (150:2.7)$);
          \draw[decorate] (u) node[vertex={above}{$\symbf{u}$}] {} -- (v) node[vertex={above}{$\symbf{v}$}] {};
        \end{tikzpicture}
      )
      = \varphi_p(\symbf{y} - \symbf{x}) \varphi_p(\symbf{v} - \symbf{u})
    \]
  \end{block}
\end{frame}

\begin{frame}{パーコレーション・クラスター ＝ a string of sausages}
  \begin{center}
    \begin{tikzpicture}[perc diagram]
      \draw (2,0) to [out=60, in=120] ++(2,0) to [out=240, in=300] ++(-2,0);
      \foreach \i in {1, ..., 7}
        \draw (4*\i,0) -- ++(2,0) to [out=60, in=120] ++(2,0) to [out=240, in=300] ++(-2,0);
    \end{tikzpicture}
    \begin{adjustbox}{max size={\textwidth}{.77\textheight}}
      \includegraphics{space-time.png}
    \end{adjustbox}
  \end{center}
\end{frame}

\section[主結果の証明]{主結果の証明}

\begin{frame}{Bootstrapping argument}
  \vspace*{-3ex}
  \begin{block}{命題~([Nguyen-Yang, 1993] あるいは [Sakai, 2001])}
    $p < p_\mathrm{c}$ に対して，展開係数 $\Pi_p(\symbf{x}) = \sum_{N=0}^{\infty} (-1)^N \pi_p^{(N)}(\symbf{x})$ が存在して，
    \[
      \varphi_p(\symbf{x}) = \KroneckerDelta{\symbf{o}}{\symbf{x}} + \Pi_p(\symbf{x}) + \qty\Big(\qty(\KroneckerDelta{\symbf{o}}{\cdot} + \Pi_p) \Conv q_p \Conv \varphi_p)(\symbf{x}).
    \]
  \end{block}
  \begin{small}
    $\implies$ 両辺のFourier-Laplace変換を取ると，
    $\flt\varphi_p(k, z) = \qty\big(1 + \flt\Pi_p(k, z)) / \qty\Big(1 - \flt q_p(k, z) \qty\big(1 + \flt\Pi_p(k, z)))$．
  \end{small}

  \begin{columns}[T]
    \begin{column}{0.60\textwidth}
      \setlength{\leftmargini}{1em}
      \begin{enumerate}
        \item $g(p, m) \coloneqq \sup\limits_{k,\, \abs{z}\in\set{1, m}} \frac{\abs{\flt\varphi_p(k, z)}}{\abs*{\flt Q_1(k, \mu_p(z))}}$，$K>1$ を取る．
        \item $(p, m) = (0, 1)$ で $g(0, 1) = 1 < K$ を確認．
        \item $\interval[open right]{0}{p_\mathrm{c}}\times\interval[open right]{1}{m_p}$ 上で $g(p, m)$ の連続性を証明．
        \item レース展開によって，$\forall p\in\interval[open]{0}{p_\mathrm{c}}$, $\forall m\in\interval[open]{1}{m_p}$, [$g(p, m)\leq K \implies g(p, m) < K$] を証明．
      \end{enumerate}
      \smallskip
      \begin{small}
        ※$\abs{\flt\Pi_p(k, z)}$ の上界は $\varphi_p(\cdot)$ を含むので，$\abs{\flt\varphi_p(k, z)}$ を直接評価することができない．
      \end{small}
    \end{column}
    \begin{column}{0.38\textwidth}
      \begin{adjustbox}{width=\textwidth}
        \includegraphics{bootstrapping.pdf}
      \end{adjustbox}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{体心立方格子上のRW量の評価}
  \begin{columns}[T]
    \begin{column}{0.30\textwidth}
      \begin{adjustbox}{width=\textwidth}
        \includegraphics{bcc.pdf}
      \end{adjustbox}
    \end{column}
    \begin{column}{0.64\textwidth}
      \begin{itemize}
        \item $\mathbb{L}^d$ が $d$ 次元体心立方格子\\
          $\overset{\mathrm{def}.}{\iff} \parbox{0.8\textwidth}{$\mathscr{N}=\set{x = (x_1, \dots, x_d)\in\mathbb{Z}^d | \prod_{j=1}^{d}\abs{x_j} = 1}$ の平行移動で生成される集合，かつ $\mathbb{L}^d\ni o$．}$
        \medskip
        \item 体心立方格子上のRWの遷移確率
          \setlength\abovedisplayskip{0pt}\setlength\belowdisplayskip{0pt}
          \[
            D(x) = \frac{1}{2^d}\indicator{\set{x\in\mathscr{N}}} = \prod_{j=1}^{d}\frac{1}{2}\KroneckerDelta{\abs{x_j}}{1}
          \]
          $\iff$ $d$ 個の独立な $1$ 次元RWの遷移確率．
      \end{itemize}
    \end{column}
  \end{columns}

  Stirlingの公式によって
  \[
    D^{\conv 2n}(o) = \qty(\binom{2n}{n} \frac{1}{2^{2n}})^d \underset{n\uparrow\infty}{\sim} \qty(\frac{1}{\sqrt{\pi n}})^d.
  \]
  これを用いると，例えば，次のようなダイアグラムが評価できる：
  \[
    \sup_{\symbf{x}} \sum_{\symbf{y}} \varphi_p(\symbf{y}) \varphi_p(\symbf{y} - \symbf{x})
    \leq \int\displaylimits_{\interval{-\pi}{\pi}^d}\frac{\dd[d]{k}}{(2\pi)^d} \int_{-\pi}^{\pi}\frac{\dd{\theta}}{2\pi} \abs{\flt\varphi_p(k, \Napier^{\imag\theta})}^2
    \leq K^2 \sum_{n=0}^{\infty} D^{\conv 2n}(o).
  \]
\end{frame}

\begin{frame}{体心立方格子に対する $g(p, m)$ の上界}
  \small
  \vspace*{-2.5ex}
  \begin{lemma}[体心立方格子では $k$ の範囲を制限できること]
    \setlength\abovedisplayskip{0pt}
    \begin{equation*}
      g(p, m)
      = \sup_{\substack{(k, \theta)\in\interval{-\pi}{\pi}^{d+1},\\ r\in\set{1, m}}}
        \frac{\abs*{\flt\varphi_p(k, r\Napier^{\imag\theta})}}{\abs*{\flt Q_1(k, \mu_p(r\Napier^{\imag\theta}))}}
      = \sup_{\substack{(k, \theta)\in\textcolor{red}{\interval{-\frac{\pi}{2}}{\frac{\pi}{2}}^d}\times\interval{-\pi}{\pi},\\ r\in\set{1, m}}}
        \frac{\abs*{\flt\varphi_p(k, r\Napier^{\imag\theta})}}{\abs*{\flt Q_1(k, \mu_p(r\Napier^{\imag\theta}))}}
    \end{equation*}
  \end{lemma}
  \begin{lemma}[制限された $k$ での下界]
    \[
      \forall r\in\interval[open left]{0}{1},\ \forall (k, \theta)\in\textcolor{red}{\interval[scaled]{-\frac{\pi}{2}}{\frac{\pi}{2}}^d}\times\interval{-\pi}{\pi},
      \qquad
      \abs{1 - r\Napier^{\imag\theta} \ft D(k)} \geq \textcolor{ForestGreen}{\frac{1}{2} \qty(\frac{\abs{\theta}}{\pi} + 1 - \ft D(k))}
    \]
  \end{lemma}

  \vspace*{-4.5ex}
  \begin{align*}
    &g(p, m) \leq \qty[\text{$\abs*{\flt\Pi_p(0, m)}$ の項}]
      + \mathrm{const}. \times \sup_{\substack{(k, \theta)\in\interval{-\frac{\pi}{2}}{\frac{\pi}{2}}^d\times\interval{-\pi}{\pi},\\ r\in\set{1, m}}}
        \underbrace{\frac{\abs*{\flt\Pi_p(0, r) - \flt\Pi_p(k, r\Napier^{\imag\theta})}}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}}_{=(\dagger)}\\
    &\begin{aligned}
      (\dagger)
      \onslide*<1>{
        &\leq \frac{\abs*{\flt\Pi_p(0, r) - \flt\Pi_p(0, r\Napier^{\imag\theta})} + \abs*{\flt\Pi_p(0, r\Napier^{\imag\theta}) - \flt\Pi_p(k, r\Napier^{\imag\theta})}}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}\\
        &\leq \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) r^t \cdot \frac{\abs{1 - \Napier^{\imag\theta t}} + 1 - \cos k\cdot x}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}
      }
      \onslide*<2>{
        &\leq \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) r^t \cdot \frac{\abs{1 - \Napier^{\imag\theta t}} + 1 - \cos k\cdot x}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}\\
        &\leq \textcolor{blue}{2} \qty\bigg(\pi \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t t)
          \vee \qty\bigg(\sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t \frac{1 - \cos k\cdot x}{1 - \ft D(k)})
      }
    \end{aligned}
  \end{align*}
\end{frame}

\begin{frame}{単純立方格子に対する $g(p, m)$ の上界}
  \small
  \vspace*{-2.5ex}
  \begin{lemma}[単純立方格子では $\theta$ の範囲を制限できること]
    \setlength\abovedisplayskip{0pt}
    \begin{equation*}
      g(p, m)
      = \sup_{\substack{(k, \theta)\in\interval{-\pi}{\pi}^{d+1},\\ r\in\set{1, m}}}
        \frac{\abs*{\flt\varphi_p(k, r\Napier^{\imag\theta})}}{\abs*{\flt Q_1(k, \mu_p(r\Napier^{\imag\theta}))}}
      = \sup_{\substack{(k, \theta)\in\interval{-\pi}{\pi}^d\times\textcolor{red}{\interval{-\frac{\pi}{2}}{\frac{\pi}{2}}},\\ r\in\set{1, m}}}
        \frac{\abs*{\flt\varphi_p(k, r\Napier^{\imag\theta})}}{\abs*{\flt Q_1(k, \mu_p(r\Napier^{\imag\theta}))}}
    \end{equation*}
  \end{lemma}
  \begin{lemma}[制限された $\theta$ での下界]
    \[
      \forall r\in\interval[open left]{0}{1},\ \forall (k, \theta)\in\interval{-\pi}{\pi}^d\times\textcolor{red}{\interval[scaled]{-\frac{\pi}{2}}{\frac{\pi}{2}}},
      \qquad
      \abs{1 - r\Napier^{\imag\theta} \ft D(k)} \geq \textcolor{ForestGreen}{\frac{\sqrt{3} - 1}{\pi} \abs{\theta} + \frac{1 - \ft D(k)}{4}}
    \]
  \end{lemma}

  \vspace*{-4.5ex}
  \begin{align*}
    &g(p, m) \leq \qty[\text{$\abs*{\flt\Pi_p(0, m)}$ の項}]
      + \mathrm{const}. \times \sup_{\substack{(k, \theta)\in\interval{-\frac{\pi}{2}}{\frac{\pi}{2}}^d\times\interval{-\pi}{\pi},\\ r\in\set{1, m}}}
        \underbrace{\frac{\abs*{\flt\Pi_p(0, r) - \flt\Pi_p(k, r\Napier^{\imag\theta})}}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}}_{=(\dagger)}\\
    &\begin{aligned}
      (\dagger)
      &\leq \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) r^t \cdot \frac{\abs{1 - \Napier^{\imag\theta t}} + 1 - \cos k\cdot x}{\abs*{1 - \mu_p(r\Napier^{\imag\theta}) \ft D(k)}}\\
      &\leq \textcolor{blue}{4} \qty\bigg(\pi \sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t t)
        \vee \qty\bigg(\sum_{N=0}^{\infty}\sum_{(x, t)} \pi_p^{(N)}(x, t) m^t \frac{1 - \cos k\cdot x}{1 - \ft D(k)})
    \end{aligned}
  \end{align*}
\end{frame}

\section[結論]{結論}

\begin{frame}{主結果\――赤外評価}
  \vspace*{-1ex}
  \begin{alertblock}{定理~(赤外評価 [Chen-Handa-K., arXiv:2106.14211])}
    単純立方格子 $\mathbb{Z}^{d\geq \textcolor{ForestGreen}{183}}\times\mathbb{Z}_+$ および\structure{体心立方格子} $\mathbb{L}^{d\geq \textcolor{ForestGreen}{9}}\times\mathbb{Z}_+$ 上の最近接有向パーコレーションに於て，$\exists K\in\interval[open]{0}{\infty}$ s.t.
    $\forall p\in\interval[open right]{0}{p_\mathrm{c}}$,
    $\forall k\in\interval{-\pi}{\pi}^d$, $\forall z\in\mathbb{C}\colon \abs{z}\in\interval[open right]{1}{m_p}$,
    \begin{equation*}
      \abs{\flt\varphi_p(k, z)}
      \leq K \abs{\flt Q_1(k, \mu_p(z))}
      = \frac{\textcolor{black}{K}}{\abs*{1 - \flt q_1(k, \mu_p(z))}}.
    \end{equation*}
  \end{alertblock}

  \medskip
  \fbox{先行研究}\hspace{0.5em}
  [Nguyen-Yang, 1993] によると，$\mathbb{Z}^d$ 上の
    $\begin{cases*}
      \hspace{-1em} & 最近接モデルの場合は $d\gg 4$ \alert{(?)}，\\
      \hspace{-1em} & spread-outモデルの場合は $d>4$．
    \end{cases*}$

  \fbox{本研究の改良点}\hspace{0.5em}
  \begin{itemize}
    \item 最近接モデルの場合に赤外評価が成り立つ次元を特定．
    \item {[Nguyen-Yang, 1993]} に於て「$\abs*{1 - \Napier^{\imag\theta} \ft D(k)}$ が，
      $\norm*{(k, \theta)}_2 \leq \epsilon$ では下から $C_1 \norm*{k}_2 + C_2 \abs*{\theta}$ によって，
      $\norm*{(k, \theta)}_2 > \epsilon$ では下から\structure{定数}によって押さえられる」とした部分を修正．
    \item \structure{重み付きバブル}の有限性の成立次元を $d>8$ [Nguyen-Yang, 1993] から $d>4$ に降下．
  \end{itemize}
\end{frame}

\begin{frame}{補足資料}
  \begin{enumerate}
    % \item パーコレーション (ordinary percolation)
    % \begin{description}\itemsep=0.3em
    %   \item[予想] 臨界次元 $d_\mathrm{c}=6$．
    %   \item[先行研究]
    %   \begin{itemize}
    %     \item 最近接モデル：Fitzner-van der HofstadがNoBLEによって $\mathbb{Z}^{d\geq 11}$ 上で証明．
    %     \item Cf., spread-outモデル：Hara-Sladeがレース展開によって $\mathbb{Z}^{d>6}$ 上で証明．
    %   \end{itemize}
    %   \item[結果] 体心立方格子 $\mathbb{L}^{\textcolor{red}{d\geq 9}}$ 上で証明．
    %   \item[共同研究者] 半田悟 氏 (atama plus Inc.)・坂井哲 教授（北大）
    %   \item[文献] \textit{Taiwan. J. Math.} \textbf{24} (2020): 723--784.
    % \end{description}
    \item 有向パーコレーション (oriented percolation)
    \begin{description}\itemsep=0.3em
      \item[予想] 臨界次元 $d_\mathrm{c}+1 = 4+1$．
      \item[先行研究] Nguyen-Yangがレース展開によって
      \begin{itemize}
        \item 最近接モデル：$\mathbb{Z}^{d\gg 4}\times\mathbb{Z}_+$ 上で証明 \alert{(?)}．
        \item Cf., spread-outモデル：$\mathbb{Z}^{d>4}\times\mathbb{Z}_+$ 上で証明．
      \end{itemize}
      \item[結果] 体心立方格子 $\mathbb{L}^{\textcolor{red}{d\geq 9}}\times\mathbb{Z}_+$ 上で証明．
      \item[共同研究者] 陳隆奇 教授（國立政治大學，台湾）・半田悟 氏 (atama plus Inc.)
      \item[文献] arXiv:2106.14211
    \end{description}
  \end{enumerate}

  \begin{columns}
    \begin{column}{0.49\textwidth}
      \centering
      \begin{tikzpicture}
        \node[inner sep=0pt] (figure) {\adjincludegraphics[max width=0.52\textwidth]{nearest-neighbor-model.pdf}};
        \node[
            rectangle callout,
            fill=cyan!20,
            callout absolute pointer={($(figure)!.5!(figure.north east)$)},
            right=0pt of figure.east,
            text width=0.48\textwidth,
            align=left
          ] {\textbf{最近接モデル} ($\mathbb{Z}^d$ では $\norm{x}_1=1$ の点へのみ遷移可能) の場合 $d\gg 4$ で証明 \alert{(?)}，};
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.49\textwidth}
      \centering
      \begin{tikzpicture}
        \node[inner sep=0pt] (figure) {\adjincludegraphics[max width=0.52\textwidth]{spread-out-model.pdf}};
        \node[
          rectangle callout,
          fill=cyan!20,
          callout absolute pointer={($(figure)!.5!(figure.north east)$)},
          right=0pt of figure.east,
          text width=0.48\textwidth,
          align=left
        ] {\textbf{spread-outモデル} ($L>0$ に対して，$\mathbb{Z}^d$ では $0<\norm{x}_\infty\leq L$ の点へのみ遷移可能) の場合 $d>4$ で証明．};
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\end{document}
